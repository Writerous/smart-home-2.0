FROM ubuntu

ENV TZ=Europe/Moscow

WORKDIR /app

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update -y
RUN apt-get install -y nodejs npm
COPY . .

EXPOSE 4200

CMD npm run start:docker
