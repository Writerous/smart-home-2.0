import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { LoadableFacade } from '@models/common';
import { RoomList } from '@models/room-list';
import { Room } from '@models/room';
import { Dictionary } from '@ngrx/entity';
import { Hardware } from '@models/hardware';
import { Equipment } from '@models/equipment';
import { RoomListState } from './reducer';
import {
	AddRoom,
	DeleteRoom,
	LoadRoomList,
	MoveHardware,
	UpdateOneHardware,
	UpdateOneHardwareFailure,
	UpdateRoom,
} from './actions';
import {
	selectCallState,
	selectEquipmentById,
	selectHardware,
	selectHardwareById,
	selectHardwares,
	selectRoom,
	selectRoomById,
	selectRoomList,
	selectRoomListEntities,
	selectRooms,
} from './selectors';
import { shareReplay } from 'rxjs/operators';

@Injectable()
export class RoomListFacade extends LoadableFacade<RoomListState> {
	//комнаты
	public readonly rooms$: Observable<Room[]>;
	//объект для работы над списком комнат
	public readonly roomListEntities$: Observable<Dictionary<Room>>;
	//список комнат
	public readonly roomList$: Observable<RoomList>;
	//комната
	public readonly room$: Observable<Room | undefined>;
	//устройство
	public readonly hardware$: Observable<Hardware | undefined>;
	public readonly hardwares$: Observable<(Hardware | undefined)[]>;

	constructor(store: Store<RoomListState>) {
		super(store, selectCallState);

		this.rooms$ = this.store.pipe(select(selectRooms), shareReplay(1));
		this.roomList$ = this.store.pipe(select(selectRoomList), shareReplay(1));
		this.roomListEntities$ = this.store.pipe(select(selectRoomListEntities), shareReplay(1));
		this.room$ = this.store.pipe(select(selectRoom), shareReplay(1));
		this.hardware$ = this.store.pipe(select(selectHardware), shareReplay(1));
		this.hardwares$ = this.store.pipe(select(selectHardwares), shareReplay(1));
	}
	//получение комнаты по id
	public roomById$(id: Room['id']): Observable<Room> {
		return this.store.pipe(select(selectRoomById, id));
	}
	//получение устройства по id
	public hardwareById$(roomId: Room['id'], hardwareId: Hardware['id']): Observable<Hardware> {
		return this.store.pipe(select(selectHardwareById, { roomId, hardwareId }));
	}
	//получение части устройства по id
	public equipmentById$(
		roomId: Room['id'],
		hardwareId: Hardware['id'],
		equipmentId: Equipment['id'],
	): Observable<Equipment> {
		return this.store.pipe(select(selectEquipmentById, { roomId, hardwareId, equipmentId }));
	}
	//обновить комнату
	public updateRoom(room: Room): void {
		this.store.dispatch(new UpdateRoom({ room }));
	}
	//добавить комнату
	public addRoom(room: Room): void {
		this.store.dispatch(new AddRoom({ room }));
	}
	//удалить комнату
	public deleteRoom(room: Room): void {
		this.store.dispatch(new DeleteRoom({ room }));
	}
	//загрузить комнаты
	public loadRooms(): void {
		this.store.dispatch(new LoadRoomList());
	}
	public updateHardware(hardware: Hardware, room: Room): void {
		this.store.dispatch(new UpdateOneHardware({ room, hardware }));
	}
	public moveHardware(roomList: RoomList): void {
		this.store.dispatch(new MoveHardware({ roomList }));
	}

	public updateHardwareFailure(): void {
		this.store.dispatch(
			new UpdateOneHardwareFailure({ errorMsg: 'Не удалось изменить устройство' }),
		);
	}
}
