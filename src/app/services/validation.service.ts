import { Injectable } from '@angular/core';
import { BaseDomain } from '@models/common';
import { Observable, of } from 'rxjs';
import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { catchError, debounceTime, map, take } from 'rxjs/operators';

const notUnique: ValidationErrors = { notUnique: true };

@Injectable()
export class ValidationService {
	uniqueValidator<T extends BaseDomain>(entities$: Observable<T[]>): AsyncValidatorFn {
		return function (control: AbstractControl): Observable<null | ValidationErrors> {
			return entities$.pipe(
				debounceTime(300),
				take(1),
				map((entities) =>
					!!entities.find((ent) => ent.name === control.value) ? notUnique : null,
				),
				catchError(() => of(notUnique)),
			);
		};
	}
}
