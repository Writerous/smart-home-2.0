import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
	AuthInterceptor,
	AuthService,
	EquipmentPartitionService,
	HttpCommandsService,
	HttpRoomsService,
	LoadingService,
	SerializeService,
	SidenavService,
	ValidationService,
} from '@services';
import { RoomListGuard } from '../ui/mainContainer/room-list/room-list-guard';
import { CommandListGuard } from '../ui/mainContainer/commands/command-list-guard';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

export const httpInterceptorProviders = [
	{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
];

@NgModule({
	declarations: [],
	imports: [CommonModule],
})
export class ServiceModule {
	static forRoot(): ModuleWithProviders<ServiceModule> {
		return {
			ngModule: ServiceModule,
			providers: [
				...httpInterceptorProviders,
				HttpRoomsService,
				EquipmentPartitionService,
				SerializeService,
				SidenavService,
				LoadingService,
				RoomListGuard,
				HttpCommandsService,
				CommandListGuard,
				AuthService,
				ValidationService,
			],
		};
	}
}
