import { TestBed } from '@angular/core/testing';

import { ValidationService } from './validation.service';
import { Observable, of } from 'rxjs';
import { FormControl, ValidationErrors } from '@angular/forms';

describe('ValidationService', () => {
	let service: ValidationService;

	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [ValidationService],
		});
		service = TestBed.inject(ValidationService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('should response not unique', (done) => {
		const targetName = 'test';
		const entities = of([
			{ name: 'lol', id: 'lo' },
			{ name: 'kek', id: 'ke' },
			{ name: targetName, id: '123' },
		]);
		const control = new FormControl();
		control.setValue(targetName);

		(service.uniqueValidator(entities)(
			control,
		) as Observable<null | ValidationErrors>).subscribe((result) => {
			if (result !== null) {
				expect(result.notUnique).toBeTruthy();
			} else {
				expect(result).toBeTruthy();
			}
			done();
		});
	});

	it('should response unique', (done) => {
		const targetName = 'test';
		const entities = of([
			{ name: 'lol', id: 'lo' },
			{ name: 'kek', id: 'ke' },
			{ name: 'cheburek', id: '123' },
		]);
		const control = new FormControl();
		control.setValue(targetName);

		(service.uniqueValidator(entities)(
			control,
		) as Observable<null | ValidationErrors>).subscribe((result) => {
			expect(result).toBeNull();
			done();
		});
	});
});
