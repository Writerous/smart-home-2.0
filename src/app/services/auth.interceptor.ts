import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { concatMap, take } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
	constructor(private readonly fireAuth: AngularFireAuth) {}

	intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
		return this.fireAuth.idToken.pipe(
			take(1),
			concatMap((idToken) => {
				if (idToken) {
					const authRequest = request.clone({
						params: request.params.set('auth', idToken),
					});
					return next.handle(authRequest);
				}

				return next.handle(request);
			}),
		);
	}
}
