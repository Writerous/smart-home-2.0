import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainContainerComponent } from './mainContainer.component';
import { AuthService, SidenavService } from '@services';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatMenuModule } from '@angular/material/menu';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';

const sidenavServiceStub: Partial<SidenavService> = {
	// eslint-disable-next-line @typescript-eslint/no-empty-function
	setState(_value: boolean) {},
	getState(): Observable<boolean> {
		return of(false);
	},
	sidenavSubject: new BehaviorSubject<boolean>(false),
};
const authServiceStub: Partial<AuthService> = {
	// eslint-disable-next-line @typescript-eslint/no-empty-function
	logout() {
		return of(true);
	},
	user$: of({ email: 'email', uid: '12345' }),
};
const breakpointObserverStub: Partial<BreakpointObserver> = {
	observe(_value: string | string[]): Observable<BreakpointState> {
		return of({ matches: true, breakpoints: { lel: true } });
	},
};

describe('MainContainer', () => {
	let component: MainContainerComponent;
	let fixture: ComponentFixture<MainContainerComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [MainContainerComponent],
			imports: [
				MatSidenavModule,
				MatListModule,
				MatIconModule,
				MatToolbarModule,
				MatButtonModule,
				MatProgressBarModule,
				OverlayModule,
				MatMenuModule,
				RouterTestingModule,
				BrowserAnimationsModule,
			],
			providers: [
				{ provide: SidenavService, useValue: sidenavServiceStub },
				{ provide: AuthService, useValue: authServiceStub },
				{ provide: BreakpointObserver, useValue: breakpointObserverStub },
			],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(MainContainerComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('title binding expectation', () => {
		const title = 'smart test home';

		component.title = title;
		fixture.detectChanges();

		const titlePlace = fixture.nativeElement.querySelector('.title');
		expect(titlePlace.textContent).toContain(title);
	});

	it('email binding expectation', () => {
		const email = 'email';

		component.ngOnInit();
		fixture.detectChanges();

		const emailPlace = fixture.nativeElement.querySelector('.email');
		expect(emailPlace.textContent).toContain(email);
	});
});
