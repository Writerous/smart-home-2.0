import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDrawerMode, MatSidenav } from '@angular/material/sidenav';
import { AuthService, SidenavService } from '@services';
import { Observable } from 'rxjs';
import { UserLoggedIn } from '@models/user';
import { filter, map } from 'rxjs/operators';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Router } from '@angular/router';
import { DestroyableDirective } from '../destroyable/destroyable.directive';

@Component({
	selector: 'app-ui',
	templateUrl: './mainContainer.component.html',
	styleUrls: ['./mainContainer.component.scss'],
})
export class MainContainerComponent extends DestroyableDirective implements OnDestroy, OnInit {
	@ViewChild('sidenav') sideNav: MatSidenav;
	user$: Observable<UserLoggedIn | null>;
	sidenavMode: Observable<MatDrawerMode>;

	title = 'Smart Home';

	sidenavLinks: { name: string; path: string }[] = [
		{
			name: 'Комнаты',
			path: 'rooms',
		},
		{
			name: 'Сценарии',
			path: 'commands',
		},
	];

	constructor(
		private readonly sidenavService: SidenavService,
		private readonly authService: AuthService,
		private readonly breakpointObserver: BreakpointObserver,
		private readonly router: Router,
	) {
		super();
	}

	public onToggle(value: boolean): void {
		this.sidenavService.setState(value);
	}

	public logOut(): void {
		this.authService
			.logout()
			.pipe(this.takeUntilDestroy)
			.subscribe(() => {
				this.router.navigate(['/login']);
			});
	}

	ngOnInit(): void {
		this.user$ = this.authService.user$.pipe(filter((user) => !!user));
		this.sidenavMode = this.breakpointObserver
			.observe([Breakpoints.XLarge, Breakpoints.Large])
			.pipe(map((result) => (result.matches ? 'side' : 'over')));
	}

	ngOnDestroy(): void {
		this.sidenavService.sidenavSubject?.complete();
		super.ngOnDestroy();
	}
}
