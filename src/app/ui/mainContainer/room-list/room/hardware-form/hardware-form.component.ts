import {
	ChangeDetectionStrategy,
	Component,
	EventEmitter,
	OnDestroy,
	OnInit,
	Output,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HardwareFormFacade } from '@store/hardware-form';
import { RoomListFacade } from '@store/room-list';
import { Room } from '@models/room';
import { map, takeUntil, withLatestFrom } from 'rxjs/operators';
import {
	AbstractControl,
	AsyncValidatorFn,
	FormBuilder,
	FormGroup,
	Validators,
} from '@angular/forms';
import { LastVisitedService, ValidationService } from '@services';
import { Hardware } from '@models/hardware';
import { RoomList } from '@models/room-list';

@Component({
	selector: 'app-hardware-form',
	templateUrl: './hardware-form.component.html',
	styleUrls: ['./hardware-form.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HardwareFormComponent implements OnDestroy, OnInit {
	public rooms$: Observable<Room[]>;
	public roomList$: Observable<RoomList>;
	public room$: Observable<Room | undefined>;
	public hardware$: Observable<Hardware | undefined>;
	public hardwareForm: FormGroup;

	private readonly destroy$ = new Subject();
	@Output('submitted') public submitEmitter = new EventEmitter();
	@Output('cancelled') public cancelEmitter = new EventEmitter();

	constructor(
		public readonly hardwareFormFacade: HardwareFormFacade,
		public readonly roomListFacade: RoomListFacade,
		private readonly fb: FormBuilder,
		private readonly validationService: ValidationService,
		private readonly lastVisitedService: LastVisitedService,
	) {}

	get nameControl(): AbstractControl | null {
		return this.hardwareForm.get('name');
	}

	get roomIdControl(): AbstractControl | null {
		return this.hardwareForm.get('roomId');
	}

	ngOnInit(): void {
		this.initObservables();
		this.initHardwareForm();
		this.populateHardwareForm();
	}

	private initObservables(): void {
		this.rooms$ = this.roomListFacade.rooms$.pipe(takeUntil(this.destroy$));
		this.roomList$ = this.roomListFacade.roomList$.pipe(takeUntil(this.destroy$));
		this.room$ = this.roomListFacade.room$.pipe(takeUntil(this.destroy$));
		this.hardware$ = this.roomListFacade.hardware$.pipe(takeUntil(this.destroy$));
	}

	initHardwareForm(): void {
		this.hardwareForm = this.fb.group({
			name: ['', Validators.required.bind(Validators), this.uniqueValidator],
			roomId: [''],
		});
	}

	private populateHardwareForm(): void {
		this.hardware$.subscribe((hardware) => {
			if (hardware) {
				this.nameControl?.setValue(hardware.name);
			}
		});

		this.room$.subscribe((room) => {
			if (room) {
				this.roomIdControl?.setValue(room.id);
			}
		});
	}

	get uniqueValidator(): AsyncValidatorFn {
		return this.validationService.uniqueValidator<Hardware>(this.otherHardwares);
	}

	get otherHardwares(): Observable<Hardware[]> {
		return this.roomListFacade.hardwares$.pipe(
			withLatestFrom(this.hardware$),
			map(([hardwares, hardware]) => {
				return hardwares.filter((hrdw) => hrdw && hardware && hardware.name !== hrdw.name);
			}),
		) as Observable<Hardware[]>;
	}

	public submitForm(): void {
		if (!this.isHardwareFormReadyToSubmit) return;

		this.submitSubscription();
		this.submitEmitter.emit();
	}

	submitSubscription(): void {
		this.renamedHardwareRoomAndRoomList.subscribe(
			([renamedHardware, room, roomList]) => {
				if (this.roomIdControl?.pristine) {
					this.submitOnlyName([renamedHardware, room]);
				} else {
					this.submitMovingHardware([renamedHardware, room, roomList]);
					this.lastVisitedService.remove(room.id as string);
				}
			},
			() => this.roomListFacade.updateHardwareFailure(),
		);
	}

	get isHardwareFormReadyToSubmit(): boolean {
		return !(
			this.hardwareForm.invalid ||
			this.hardwareForm.pending ||
			this.hardwareForm.pristine
		);
	}

	submitOnlyName([renamedHardware, room]: [Hardware, Room]): void {
		this.roomListFacade.updateHardware(renamedHardware, room);
	}

	get renamedHardware(): Observable<Hardware> {
		return this.hardware$.pipe(
			map((hardware) => {
				const name = this.nameControl?.value;

				if (hardware && name) return new Hardware({ ...hardware, name });

				throw Error;
			}),
		);
	}

	get renamedHardwareWithRoom(): Observable<[Hardware, Room]> {
		return this.renamedHardware.pipe(
			withLatestFrom(this.room$),
			map(([hardware, room]) => {
				if (!hardware || !room) throw Error;

				return [hardware, Room.updateHardware(room, hardware)];
			}),
		);
	}

	get renamedHardwareRoomAndRoomList(): Observable<[Hardware, Room, RoomList]> {
		return this.renamedHardwareWithRoom.pipe(
			withLatestFrom(this.roomList$),
			map(([[hardware, room], roomList]) => {
				if ([hardware, room, roomList].some((items) => items === undefined)) throw Error;

				return [hardware, room, roomList] as [Hardware, Room, RoomList];
			}),
		);
	}

	submitMovingHardware([hardware, room, roomList]: [Hardware, Room, RoomList]): void {
		const id = this.roomIdControl?.value;

		const sourceRoom = Room.deleteHardware(room, hardware);
		const targetRoom = Room.addHardware(RoomList.getRoom(roomList, id) as Room, hardware);

		const updatedRoomList = RoomList.updateManyRooms(roomList, [targetRoom, sourceRoom]);

		this.roomListFacade.moveHardware(updatedRoomList);
	}

	public reset(): void {
		this.hardwareForm.reset();
		this.cancelEmitter.emit();
	}

	ngOnDestroy(): void {
		this.destroy$.next();
		this.destroy$.complete();
	}
}
