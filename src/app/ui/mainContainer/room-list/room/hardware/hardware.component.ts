import { Component, OnInit, TemplateRef } from '@angular/core';
import { Observable } from 'rxjs';
import { Hardware } from '@models/hardware';
import { Equipment } from '@models/equipment';
import { EquipmentFormFacade } from '@store/equipment-form';
import { RoomListFacade } from '@store/room-list';
import { Room } from '@models/room';
import { MatButton } from '@angular/material/button';
import { PopupService } from '../../../../popup/popup.component';

@Component({
	selector: 'app-hardware',
	templateUrl: './hardware.component.html',
	styleUrls: ['./hardware.component.scss'],
})
export class HardwareComponent implements OnInit {
	public hardware$: Observable<Hardware | undefined>;
	public room$: Observable<Room | undefined>;

	constructor(
		public readonly equipmentFormFacade: EquipmentFormFacade,
		public readonly roomListFacade: RoomListFacade,
		private readonly popupService: PopupService,
	) {}

	ngOnInit(): void {
		this.hardware$ = this.roomListFacade.hardware$;
		this.room$ = this.roomListFacade.room$;
	}

	openForm(originTmpl: MatButton, content: TemplateRef<any>): void {
		const origin = originTmpl._elementRef.nativeElement;

		this.popupService.open({ origin, content });
	}

	onOpen(equipment: Equipment): void {
		this.equipmentFormFacade.loadEquipmentForm(equipment);
	}

	getEquipments(hardware: Hardware): (Equipment | undefined)[] {
		return Hardware.getEquipments(hardware);
	}
}
