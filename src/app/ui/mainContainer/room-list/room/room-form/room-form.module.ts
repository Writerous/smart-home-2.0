import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgrxMaterialModule } from '@helpers';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { NgrxFormsModule } from 'ngrx-forms';
import { RoomFormComponent } from './room-form.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
	declarations: [RoomFormComponent],
	imports: [
		CommonModule,
		NgrxMaterialModule,
		MatInputModule,
		MatButtonModule,
		NgrxFormsModule,
		ReactiveFormsModule,
	],
	exports: [RoomFormComponent],
})
export class RoomFormModule {}
