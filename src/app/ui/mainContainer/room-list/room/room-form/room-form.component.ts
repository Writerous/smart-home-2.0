import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil, withLatestFrom } from 'rxjs/operators';
import { Room } from '@models/room';
import { RoomListFacade } from '@store/room-list';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationService } from '@services';

@Component({
	selector: 'app-room-form',
	templateUrl: './room-form.component.html',
	styleUrls: ['./room-form.component.scss'],
})
export class RoomFormComponent implements OnDestroy, OnInit {
	public roomForm: FormGroup;

	private readonly destroy$ = new Subject();
	@Output('submitted') public submitEmitter = new EventEmitter();
	@Output('cancelled') public cancelEmitter = new EventEmitter();

	public room$: Observable<Room | undefined>;

	constructor(
		public readonly roomListFacade: RoomListFacade,
		private readonly fb: FormBuilder,
		private readonly validationService: ValidationService,
	) {
		this.roomForm = this.fb.group({
			name: ['', Validators.required.bind(Validators)],
		});

		this.room$ = this.roomListFacade.room$.pipe(takeUntil(this.destroy$));
	}

	get nameControl(): AbstractControl | null {
		return this.roomForm.get('name');
	}

	ngOnInit(): void {
		this.room$.subscribe((room) => {
			if (room) {
				this.nameControl?.setValue(room.name);
			}
		});

		const otherRooms$ = this.roomListFacade.rooms$.pipe(
			withLatestFrom(this.room$),
			map(([rooms, room]) => rooms.filter(({ id }) => room?.id !== id)),
		);

		this.nameControl?.setAsyncValidators(
			this.validationService.uniqueValidator<Room>(otherRooms$),
		);
	}

	submitForm(): void {
		if (this.roomForm.invalid && this.roomForm.pristine) {
			return;
		}

		this.room$.subscribe((room) => {
			const name = this.nameControl?.value;

			if (name && room) {
				this.roomListFacade.updateRoom(new Room({ ...room, name }));
				this.submitEmitter.emit();
			}
		});
	}

	reset(): void {
		this.roomForm.reset();
		this.cancelEmitter.emit();
	}

	ngOnDestroy(): void {
		this.destroy$.next();
		this.destroy$.complete();
	}
}
