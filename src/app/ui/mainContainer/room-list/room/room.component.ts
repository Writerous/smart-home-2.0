import { Component, OnInit, TemplateRef } from '@angular/core';
import { Observable } from 'rxjs';
import { Room } from '@models/room';
import { RoomListFacade } from '@store/room-list';
import { Hardware } from '@models/hardware';
import { PopupService } from '../../../popup/popup.component';
import { MatButton } from '@angular/material/button';

@Component({
	selector: 'app-room',
	templateUrl: './room.component.html',
	styleUrls: ['./room.component.scss'],
})
export class RoomComponent implements OnInit {
	public room$: Observable<Room | undefined>;
	public openedId: string | null = null;

	constructor(
		private readonly roomListFacade: RoomListFacade,
		private readonly popupService: PopupService,
	) {}

	ngOnInit(): void {
		this.room$ = this.roomListFacade.room$;
	}

	delete(room: Room): void {
		this.roomListFacade.deleteRoom(room);
	}

	getHardwares(room: Room): (Hardware | undefined)[] {
		return Room.getHardwares(room);
	}

	open(originTmpl: MatButton, content: TemplateRef<any>): void {
		const origin = originTmpl._elementRef.nativeElement;

		this.popupService.open({ origin, content });
	}
}
