import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { RoomListFacade, RoomListStoreActions } from '@store/room-list';
import { MatDialogRef } from '@angular/material/dialog';
import { take, takeUntil } from 'rxjs/operators';
import { Room } from '@models/room';
import { Actions, ofType } from '@ngrx/effects';
import { ValidationService } from '@services';

@Component({
	selector: 'app-add-room',
	templateUrl: './add-room.component.html',
	styleUrls: ['./add-room.component.scss'],
})
export class AddRoomComponent implements OnInit, OnDestroy {
	public destroy$ = new Subject();

	public addForm = new FormGroup({
		nameControl: new FormControl('', Validators.required.bind(Validators)),
	});

	get nameControl(): AbstractControl | null {
		return this.addForm.get('nameControl');
	}

	constructor(
		public readonly roomListFacade: RoomListFacade,
		public readonly actions$: Actions,
		public readonly dialogRef: MatDialogRef<AddRoomComponent>,
		private readonly validationService: ValidationService,
	) {}

	ngOnInit(): void {
		this.nameControl?.setAsyncValidators(
			this.validationService.uniqueValidator<Room>(this.roomListFacade.rooms$),
		);
	}

	submitForm(): void {
		if (this.addForm.valid && this.addForm.dirty) {
			const name = this.nameControl?.value;
			const room = new Room({
				...Room.initial,
				name,
			});
			this.roomListFacade.addRoom(room);
			this.checkAndClose(name);
		}
	}

	checkAndClose(name: Room['name']): void {
		this.actions$
			.pipe(
				ofType<RoomListStoreActions.AddRoomSuccess>(
					RoomListStoreActions.RoomListActionsTypes.addRoomSuccess,
				),
				take(1),
				takeUntil(this.destroy$),
			)
			.subscribe((action) => {
				if (action.payload.room.name === name) {
					this.dialogRef.close();
				}
			});
	}

	ngOnDestroy(): void {
		this.destroy$.next();
		this.destroy$.complete();
	}
}
