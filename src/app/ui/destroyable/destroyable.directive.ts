import { Directive, OnDestroy } from '@angular/core';
import { OperatorFunction, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Directive({
	selector: '[appDestroyable]',
})
export class DestroyableDirective implements OnDestroy {
	destroy$ = new Subject();

	get takeUntilDestroy(): OperatorFunction<any, any> {
		return takeUntil(this.destroy$);
	}

	ngOnDestroy(): void {
		this.destroy$.next();
		this.destroy$.complete();
	}
}
