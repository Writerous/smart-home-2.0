import {
	AfterContentInit,
	Component,
	Inject,
	Injectable,
	Injector,
	OnInit,
	Optional,
	TemplateRef,
	ViewContainerRef,
} from '@angular/core';
import {
	ConnectionPositionPair,
	Overlay,
	OverlayConfig,
	OverlayRef,
	PositionStrategy,
} from '@angular/cdk/overlay';
import {
	ComponentPortal,
	ComponentType,
	PortalInjector,
	TemplatePortal,
} from '@angular/cdk/portal';
import { find, take, takeUntil } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { animate, AnimationEvent, state, style, transition, trigger } from '@angular/animations';
import {
	_INTERNAL_POPUP_DATA,
	ANIMATION_TIMINGS,
	ESCAPE,
	POPUP_DATA,
	POPUP_DEFAULT_CONFIG,
	PopupConfig,
	PopupContent,
	PopupParams,
	positionFallbackStrategy,
} from './popup.declarations';

export class PopupRemote<Cmp> {
	constructor(private overlayRef: OverlayRef, public content: PopupContent) {}

	close(): void {
		this.setBeforeClosed();

		this.animationStarted.pipe(find((event) => event.toState === 'leave')).subscribe(() => {
			this.overlayRef.detachBackdrop();
		});
		this.animationDone.pipe(find((event) => event.toState === 'leave')).subscribe(() => {
			this.overlayRef.dispose();
		});

		this.overlayRef
			.detachments()
			.pipe(take(1))
			.subscribe(() => {
				this.setAfterClosed();
				this.animationComplete();
			});
	}

	private _beforeClose = new Subject<void>();
	private _afterClosed = new Subject<void>();
	private _animationStarted = new Subject<AnimationEvent>();
	private _animationDone = new Subject<AnimationEvent>();

	public backdropClickEvent(): Observable<MouseEvent> {
		return this.overlayRef.backdropClick();
	}

	get afterClosed(): Observable<void> {
		return this._afterClosed.pipe();
	}

	get beforeClose(): Observable<void> {
		return this._beforeClose.pipe();
	}

	get animationStarted(): Observable<AnimationEvent> {
		return this._animationStarted.pipe();
	}

	get animationDone(): Observable<AnimationEvent> {
		return this._animationDone.pipe();
	}

	setAfterClosed(): void {
		this._afterClosed.next();
		this._afterClosed.complete();
	}

	setBeforeClosed(): void {
		this._beforeClose.next();
		this._beforeClose.complete();
	}

	setAnimationStarted(event: AnimationEvent): void {
		this._animationStarted.next(event);
	}

	setAnimationDone(event: AnimationEvent): void {
		this._animationDone.next(event);
	}

	animationComplete(): void {
		this._animationStarted.complete();
		this._animationDone.complete();
	}
}

@Component({
	selector: 'app-popup',
	templateUrl: './popup.component.html',
	styleUrls: ['./popup.component.scss'],
	animations: [
		trigger('fade', [
			state('fadeOut', style({ opacity: 0 })),
			state('fadeIn', style({ opacity: 1 })),
			transition('* => fadeIn', animate(ANIMATION_TIMINGS)),
		]),
		trigger('slidePopup', [
			state('void', style({ transform: 'translateY(-5%)', opacity: 0 })),
			state('enter', style({ transform: 'translateY(0)', opacity: 1 })),
			state('leave', style({ transform: 'translateY(-5%)', opacity: 0 })),
			transition('* => *', animate(ANIMATION_TIMINGS)),
		]),
	],
})
export class PopupComponent<Content> implements AfterContentInit, OnInit {
	public portal: ComponentPortal<Content> | TemplatePortal;
	animationState: 'void' | 'enter' | 'leave' = 'enter';
	renderMethod: 'template' | 'component' = 'component';
	context;

	constructor(
		@Optional()
		@Inject(_INTERNAL_POPUP_DATA)
		public data: any,
		private popupRemote: PopupRemote<Content>,
		private injector: Injector,
		private _vcr: ViewContainerRef,
	) {}

	ngOnInit(): void {
		if (this.popupRemote.content instanceof TemplateRef) {
			this.renderMethod = 'template';
			this.context = {
				close: this.popupRemote.close.bind(this.popupRemote),
				data: this.data,
			};
		}
	}

	ngAfterContentInit(): void {
		const injector = this.createInjector();

		if (this.renderMethod === 'component') {
			this.portal = new ComponentPortal<Content>(
				this.popupRemote.content as ComponentType<Content>,
				null,
				injector,
			);
		} else {
			this.portal = new TemplatePortal(
				this.popupRemote.content as TemplateRef<any>,
				this._vcr,
				this.context,
			);
		}

		this.popupRemote.beforeClose.pipe(take(1)).subscribe(() => {
			this.startExitAnimation();
		});
	}

	private createInjector(): PortalInjector {
		// Instantiate new WeakMap for our custom injection tokens
		const injectionTokens = new WeakMap();

		// Set custom injection tokens
		injectionTokens.set(POPUP_DATA, this.data);
		injectionTokens.set(PopupRemote, this.popupRemote);

		// Instantiate new PortalInjector
		return new PortalInjector(this.injector, injectionTokens);
	}

	startExitAnimation(): void {
		this.animationState = 'leave';
	}

	onAnimationStart(event: AnimationEvent): void {
		this.popupRemote.setAnimationStarted(event);
	}

	onAnimationDone(event: AnimationEvent): void {
		this.popupRemote.setAnimationDone(event);
	}
}

@Injectable({
	providedIn: 'root',
})
export class PopupService {
	static DEFAULT_CONFIG: PopupConfig = {
		hasBackdrop: true,
		//backdropClass: 'cdk-overlay-transparent-backdrop',
		//panelClass: 'cdk-overlay-curtain-panel',
		keycodesForClose: [ESCAPE],
		closeStrategy: 'all',
		direction: 'bottom',
	};

	constructor(
		private readonly overlay: Overlay,
		private readonly injector: Injector,
		@Optional()
		@Inject(POPUP_DEFAULT_CONFIG)
		private readonly defaultConfig: PopupConfig,
	) {
		if (this.defaultConfig === null) {
			this.defaultConfig = PopupService.DEFAULT_CONFIG;
		}
	}

	public open<Cmp = any>({ origin, config, data, content }: PopupParams): PopupRemote<Cmp> {
		const mergedConfig: PopupConfig = { ...this.defaultConfig, ...config };
		const overlayRef = this.overlay.create(this.getOverlayConfig(origin, mergedConfig));
		const popupRemote = new PopupRemote(overlayRef, content);
		const injector = this.createInjector(data, popupRemote);

		overlayRef.attach(new ComponentPortal(PopupComponent, null, injector));

		this.setCloseStrategy(overlayRef, mergedConfig, popupRemote);

		return popupRemote;
	}

	private setCloseStrategy<Cmp>(
		overlayRef: OverlayRef,
		config: PopupConfig,
		remote: PopupRemote<Cmp>,
	): void {
		if (!overlayRef?.hasAttached()) {
			return;
		}

		switch (config.closeStrategy) {
			case 'onlyKey':
				this.subscribeToKey(overlayRef, config, remote);
				break;
			case 'onlyBackdrop':
				this.subscribeToBackdropClick(overlayRef, remote);
				break;
			case 'all':
				this.subscribeToKey(overlayRef, config, remote);
				this.subscribeToBackdropClick(overlayRef, remote);
		}
	}

	private subscribeToKey<Cmp>(
		overlayRef: OverlayRef,
		config: PopupConfig,
		remote: PopupRemote<Cmp>,
	): void {
		overlayRef
			.keydownEvents()
			.pipe(
				find(({ key }) => !!config.keycodesForClose?.includes(key)),
				takeUntil(remote.afterClosed),
			)
			.subscribe((_event: KeyboardEvent) => remote.close());
	}

	private subscribeToBackdropClick<Cmp>(overlayRef: OverlayRef, remote: PopupRemote<Cmp>): void {
		overlayRef
			.backdropClick()
			.pipe(take(1), takeUntil(remote.afterClosed))
			.subscribe((_) => remote.close());
	}

	private getOverlayConfig(origin: HTMLElement, config: PopupConfig): OverlayConfig {
		return new OverlayConfig({
			positionStrategy: this.getOverlayPosition(origin, config),
			hasBackdrop: config.hasBackdrop,
			backdropClass: config.backdropClass,
			panelClass: config.panelClass,
		});
	}

	private getOverlayPosition(origin: HTMLElement, config: PopupConfig): PositionStrategy {
		const positions: ConnectionPositionPair[] = positionFallbackStrategy[config.direction];
		const positionStrategy = this.overlay
			.position()
			.flexibleConnectedTo(origin)
			.withPositions(positions)
			.withPush(false);

		return positionStrategy;
	}

	private createInjector<Cmp>(data: any, popupRef: PopupRemote<Cmp>): PortalInjector {
		// Instantiate new WeakMap for our custom injection tokens
		const injectionTokens = new WeakMap();

		// Set custom injection tokens
		injectionTokens.set(PopupRemote, popupRef);
		injectionTokens.set(_INTERNAL_POPUP_DATA, data);

		// Instantiate new PortalInjector
		return new PortalInjector(this.injector, injectionTokens);
	}
}
