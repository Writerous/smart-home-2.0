import { InjectionToken, TemplateRef } from '@angular/core';
import { ComponentType } from '@angular/cdk/portal';
import { ConnectionPositionPair } from '@angular/cdk/overlay';

export interface PopupParams {
	config?: PopupConfig;
	origin: HTMLElement;
	content: PopupContent;
	data?: any;
}

export type PopupContent<Content = any> = ComponentType<Content> | TemplateRef<Content>;

export interface PopupConfig {
	hasBackdrop?: boolean;
	backdropClass?: string;
	panelClass?: string;
	keycodesForClose?: string[];
	closeStrategy?: 'onlyBackdrop' | 'onlyKey' | 'all' | 'noop';
	direction: 'top' | 'bottom' | 'left' | 'right';
}

export const ANIMATION_TIMINGS = '400ms cubic-bezier(0.25, 0.8, 0.25, 1)';
export const ESCAPE = 'Escape';
export const POPUP_DATA = new InjectionToken<any>('popup_data');
export const _INTERNAL_POPUP_DATA = new InjectionToken<any>('internal_popup_data');
export const POPUP_DEFAULT_CONFIG = new InjectionToken<PopupConfig>('popup_config');

interface PositionMap {
	top: ConnectionPositionPair;
	right: ConnectionPositionPair;
	bottom: ConnectionPositionPair;
	left: ConnectionPositionPair;
}

interface PositionFallbackStrategy {
	top: ConnectionPositionPair[];
	right: ConnectionPositionPair[];
	bottom: ConnectionPositionPair[];
	left: ConnectionPositionPair[];
}

export const posMap: PositionMap = {
	top: {
		originX: 'center',
		originY: 'top',
		overlayX: 'center',
		overlayY: 'bottom',
	},
	right: {
		originX: 'end',
		originY: 'center',
		overlayX: 'start',
		overlayY: 'center',
	},
	bottom: {
		originX: 'center',
		originY: 'bottom',
		overlayX: 'center',
		overlayY: 'top',
	},
	left: {
		originX: 'start',
		originY: 'center',
		overlayX: 'end',
		overlayY: 'center',
	},
};

export const positionFallbackStrategy: PositionFallbackStrategy = {
	top: [posMap.top, posMap.bottom, posMap.left, posMap.right],
	bottom: [posMap.bottom, posMap.top, posMap.left, posMap.right],
	left: [posMap.left, posMap.right, posMap.bottom, posMap.top],
	right: [posMap.right, posMap.left, posMap.top, posMap.bottom],
};
