module.exports = {
	preset: 'jest-preset-angular',
	setupFilesAfterEnv: ['<rootDir>/setupJest.ts'],
	transform: {
		'^.+\\.(ts|js|html)$': 'ts-jest',
	},
	moduleFileExtensions: ['ts', 'js', 'html'],
	moduleNameMapper: {
		'^@services$': '<rootDir>/src/app/services/',
		'^@models/(.*)$': '<rootDir>/src/app/models/$1',
		'^@store$': '<rootDir>/src/app/store/',
		'^@store/(.*)$': '<rootDir>/src/app/store/$1',
		'^@pipes$': '<rootDir>/src/app/pipes/',
		'^@helpers$': '<rootDir>/src/app/helpers/',
	},
};
